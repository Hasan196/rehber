/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorel.rehber.rehber.gui;

import com.toedter.calendar.JDateChooser;
import com.vektorel.rehber.bo.KisiBO;
import com.vektorel.rehber.bo.boimpl.CinsiyetBOImpl;
import com.vektorel.rehber.bo.boimpl.IlBOImpl;
import com.vektorel.rehber.bo.boimpl.IlceBOImpl;
import com.vektorel.rehber.bo.boimpl.KisiBOImpl;
import com.vektorel.rehber.db.TException;
import com.vektorel.rehber.vo.CinsiyetVO;
import com.vektorel.rehber.vo.IlVO;
import com.vektorel.rehber.vo.IlceVO;
import java.sql.SQLException;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Hasan
 */
public class DlgRehberEkle extends javax.swing.JDialog {

    /**
     * Creates new form DlgRehberEkle
     */
    public DlgRehberEkle(java.awt.Frame parent, boolean modal) throws TException, SQLException {
        super(parent, modal);
        initComponents();

        cmbDoldur();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel9 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        cmbCinsiyet = new javax.swing.JComboBox<>();
        cmbIlce = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        cmbIl = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtSoyad = new javax.swing.JTextField();
        txtAd = new javax.swing.JTextField();
        txtAdres = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtTelNo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        dateDogumTarihi = new com.toedter.calendar.JDateChooser();

        jLabel9.setText("jLabel9");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("REHBER EKLE");

        jLabel2.setText("SOYAD");

        jLabel1.setText("AD");

        jButton2.setText("KAYDET");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel4.setText("İLÇE");

        jLabel6.setText("D.TARİHİ");

        jLabel7.setText("CİNSİYET");

        jButton1.setText("İPTAL");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setText("ADRES");

        jLabel8.setText("TEL NO");

        jLabel3.setText("İL");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 96, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtAdres, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                            .addComponent(cmbIlce, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAd)
                            .addComponent(txtSoyad)
                            .addComponent(cmbCinsiyet, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbIl, javax.swing.GroupLayout.Alignment.TRAILING, 0, 136, Short.MAX_VALUE)
                            .addComponent(txtTelNo, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                            .addComponent(dateDogumTarihi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtAd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtSoyad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cmbCinsiyet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmbIl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbIlce, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtAdres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(dateDogumTarihi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtTelNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmbCinsiyet, cmbIl, cmbIlce, jButton1, jButton2, jLabel1, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6, jLabel7, jLabel8, txtAd, txtAdres, txtSoyad, txtTelNo});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(133, 133, 133)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(127, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        String ad = txtAd.getText();
        String soyad = txtSoyad.getText();
        String il = String.valueOf(cmbIl.getSelectedItem().toString());
        String ilce = String.valueOf(cmbIlce.getSelectedItem().toString());
        String cinsiyet = String.valueOf(cmbCinsiyet.getSelectedItem().toString());
        String adres = txtAdres.getText();
        String telNo = txtTelNo.getText();
        java.sql.Date dogumTarihi = new java.sql.Date(dateDogumTarihi.getDate().getTime());

        if (ad.equals("")) {
            JOptionPane.showMessageDialog(null, "Lütfen İsmi Giriniz");
        } else if (soyad.equals("")) {
            JOptionPane.showMessageDialog(null, "Lütfen Soyad");
        } else if (il.equals("")) {
            JOptionPane.showMessageDialog(null, "Lütfen İl Seçiniz");
        } else if (ilce.equals("")) {
            JOptionPane.showMessageDialog(null, "Lütfen İlçe Seçiniz");
        } else if (cinsiyet.equals("")) {
            JOptionPane.showMessageDialog(null, "Lütfen Cinsiyet Giriniz");
        } else if (adres.equals("")) {
            JOptionPane.showMessageDialog(null, "Lütfen Adres Giriniz");
        } else if (telNo.equals("")) {
            JOptionPane.showMessageDialog(null, "Lütfen Telefon Giriniz");
        } else if (dogumTarihi == null) {
            JOptionPane.showMessageDialog(null, "Lütfen Doğum Tarihi Giriniz");
        } else {

            try {
                CinsiyetBOImpl cinsiyetBOImpl = new CinsiyetBOImpl();
                CinsiyetVO cinsiyetVO = cinsiyetBOImpl.getByKod(cinsiyet);
                int cinsiyetId = cinsiyetVO.getId();

                IlBOImpl ilBOImpl = new IlBOImpl();
                IlVO ilVO = ilBOImpl.getByKod(il);
                int ilId = ilVO.getId();

                IlceBOImpl ilceBOImpl = new IlceBOImpl();
                IlceVO ilceVO = ilceBOImpl.getByKod(ilce);
                int ilceId = ilceVO.getId();

                KisiBOImpl kisiBOImpl = new KisiBOImpl();

                java.sql.Date sqlDate = new java.sql.Date(dogumTarihi.getTime());

                kisiBOImpl.addKisi(ad, soyad, String.valueOf(ilId), String.valueOf(ilceId), String.valueOf(cinsiyetId), sqlDate, telNo, adres);
                JOptionPane.showMessageDialog(this, "Kaydınız Rehbere Başarılıyla Kaydedilmiştir.");
                this.dispose();
                FrmRehberIslemleri frmRehberIslemleri = new FrmRehberIslemleri();
              //  frmRehberIslemleri.show();
                frmRehberIslemleri.tabloDoldur();
            } catch (SQLException ex) {
                Logger.getLogger(DlgRehberEkle.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TException ex) {
                Logger.getLogger(DlgRehberEkle.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DlgRehberEkle.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DlgRehberEkle.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DlgRehberEkle.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DlgRehberEkle.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DlgRehberEkle dialog = null;
                try {
                    dialog = new DlgRehberEkle(new javax.swing.JFrame(), true);
                } catch (TException ex) {
                    Logger.getLogger(DlgRehberEkle.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(DlgRehberEkle.class.getName()).log(Level.SEVERE, null, ex);
                }
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    public void cmbDoldur() throws TException, SQLException {
        CinsiyetBOImpl cinsiyetler = new CinsiyetBOImpl();
        IlBOImpl iller = new IlBOImpl();
        IlceBOImpl ilceler = new IlceBOImpl();

        // İlleri Getir
        Vector<String> listIller = new Vector<String>();
        for (int i = 0; i < iller.getAll().size(); i++) {
            listIller.add(iller.getAll().elementAt(i).getKod());
        }

        Vector<String> listIlceler = new Vector<String>();
        for (int i = 0; i < ilceler.getAll().size(); i++) {
            listIlceler.add(ilceler.getAll().elementAt(i).getKod());
        }

        Vector<String> listCinsiyetler = new Vector<String>();
        for (int i = 0; i < cinsiyetler.getAll().size(); i++) {
            listCinsiyetler.add(cinsiyetler.getAll().elementAt(i).getKod());
        }

        /* for (int i = 0; i < listIller.size(); i++) {
                            System.out.println(listIller.get(i));
                    }
         */
        cmbCinsiyet.setModel(new DefaultComboBoxModel(listCinsiyetler));
        cmbIl.setModel(new DefaultComboBoxModel(listIller));
        cmbIlce.setModel(new DefaultComboBoxModel(listIlceler));

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmbCinsiyet;
    private javax.swing.JComboBox<String> cmbIl;
    private javax.swing.JComboBox<String> cmbIlce;
    private com.toedter.calendar.JDateChooser dateDogumTarihi;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtAd;
    private javax.swing.JTextField txtAdres;
    private javax.swing.JTextField txtSoyad;
    private javax.swing.JTextField txtTelNo;
    // End of variables declaration//GEN-END:variables
}
