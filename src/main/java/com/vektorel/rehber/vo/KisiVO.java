package com.vektorel.rehber.vo;

import java.util.Date;

public class KisiVO {

    private int id;
    private String ad;
    private String soyad;
    private String il;
    private String ilce;
    private String cinsiyet;
    private Date dogumTarihi;
    private String telNo;
    private String adres;

    public KisiVO() {
    }

    public KisiVO(int id, String ad, String soyad, String il, String ilce,
            String cinsiyet, Date dogumTarihi, String telNo, String adres) {
        super();
        this.id = id;
        this.ad = ad;
        this.soyad = soyad;
        this.il = il;
        this.ilce = ilce;
        this.cinsiyet = cinsiyet;
        this.dogumTarihi = dogumTarihi;
        this.telNo = telNo;
        this.adres = adres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getIl() {
        return il;
    }

    public void setIl(String il) {
        this.il = il;
    }

    public String getIlce() {
        return ilce;
    }

    public void setIlce(String ilce) {
        this.ilce = ilce;
    }

    public String getCinsiyet() {
        return cinsiyet;
    }

    public void setCinsiyet(String cinsiyet) {
        this.cinsiyet = cinsiyet;
    }

    public Date getDogumTarihi() {
        return dogumTarihi;
    }

    public void setDogumTarihi(Date dogumTarihi) {
        this.dogumTarihi = dogumTarihi;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

}
