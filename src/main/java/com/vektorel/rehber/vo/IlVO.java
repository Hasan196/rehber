package com.vektorel.rehber.vo;

public class IlVO {
	private int id;
	private String tanim;
	private String kod;
	
	public IlVO() {
		// TODO Auto-generated constructor stub
	}
	
	public IlVO(int id, String tanim, String kod) {
		super();
		this.id = id;
		this.tanim = tanim;
		this.kod = kod;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTanim() {
		return tanim;
	}
	public void setTanim(String tanim) {
		this.tanim = tanim;
	}
	public String getKod() {
		return kod;
	}
	public void setKod(String kod) {
		this.kod = kod;
	}
	
}
