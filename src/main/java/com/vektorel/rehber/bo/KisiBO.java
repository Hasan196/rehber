package com.vektorel.rehber.bo;

import java.util.Date;

import com.vektorel.rehber.vo.KisiVO;
import com.vektorel.rehber.db.TException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;


public interface KisiBO {
	
	public void addKisi( String ad, String soyad, String il, String ilce,String cinsiyet, Date dogumTarihi, String telNo, String adres)  throws SQLException,TException;
	public void updateKisi(KisiVO kisi);
	public void deleteKisi(KisiVO kisi);
	public Vector<KisiVO> getAll() throws SQLException,TException;
	public ResultSet getAllResultSet() throws SQLException,TException;
	public KisiVO getById(long id);
	public Vector<KisiVO> arama(String kelime);
}
