/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorel.rehber.bo;

import com.vektorel.rehber.vo.CinsiyetVO;
import com.vektorel.rehber.db.TException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Hasan
 */
public interface CinsiyetBO {
	public List<CinsiyetVO> getAll() throws SQLException,TException;
	public CinsiyetVO getByKod(String kod)  throws SQLException, TException ;    
}
