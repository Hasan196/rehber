/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorel.rehber.bo;

import com.vektorel.rehber.vo.CinsiyetVO;
import com.vektorel.rehber.db.TException;
import com.vektorel.rehber.vo.IlVO;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Hasan
 */
public interface IlBO {
	public Vector<IlVO> getAll() throws SQLException,TException;
	public IlVO getByKod(String kod)  throws SQLException, TException ;    
}
