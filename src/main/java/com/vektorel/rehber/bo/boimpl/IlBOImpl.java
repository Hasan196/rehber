/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorel.rehber.bo.boimpl;

import com.vektorel.rehber.bo.IlBO;
import com.vektorel.rehber.db.DBConnection;
import com.vektorel.rehber.db.TException;
import com.vektorel.rehber.vo.IlVO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hasan
 */
public class IlBOImpl implements IlBO {

    public Vector<IlVO> getAll() throws SQLException, TException {
        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM GNL_IL";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            rs = preparedStatement.executeQuery();
            Vector<IlVO> iller = new Vector<IlVO>();

            while (rs.next()) {
                IlVO il = new IlVO();
                il.setKod(rs.getString("kod"));
                iller.add(il);
            }
            return iller;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            con.close();
            dbConnection.baglantiSonlandir();
        }
        return null;
    }

    public IlVO getByKod(String kod)  throws SQLException, TException  {

        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM GNL_IL WHERE KOD=?";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, kod);
            rs = preparedStatement.executeQuery();
            IlVO iller = new IlVO();
            while (rs.next()) {              
                iller.setKod(rs.getString("kod"));                
                iller.setId(Integer.valueOf(rs.getString("id")));                
                iller.setTanim(rs.getString("tanim"));                
            }
            return iller;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            con.close();
            dbConnection.baglantiSonlandir();
        }
        return null;
    }


}
