/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorel.rehber.bo.boimpl;

import com.vektorel.rehber.bo.IlceBO;
import com.vektorel.rehber.db.DBConnection;
import com.vektorel.rehber.db.TException;
import com.vektorel.rehber.vo.IlVO;
import com.vektorel.rehber.vo.IlceVO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hasan
 */
public class IlceBOImpl implements IlceBO {

    public Vector<IlceVO> getAll() throws SQLException, TException {
        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM GNL_ILCE";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            rs = preparedStatement.executeQuery();
            Vector<IlceVO> ilceler = new Vector<IlceVO>();

            while (rs.next()) {
                IlceVO il = new IlceVO();
                il.setKod(rs.getString("kod"));
                ilceler.add(il);
            }
            return ilceler;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            preparedStatement.close();
            rs.close();
            con.close();
            dbConnection.baglantiSonlandir();
        }
        return null;
    }

    public IlceVO getByKod(String kod)  throws SQLException, TException  {
        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM GNL_ILCE WHERE KOD=?";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, kod);
            rs = preparedStatement.executeQuery();
            IlceVO ilceler = new IlceVO();
            while (rs.next()) {              
                ilceler.setKod(rs.getString("kod"));                
                ilceler.setId(Integer.valueOf(rs.getString("id")));                
                ilceler.setTanim(rs.getString("tanim"));                
            }
            return ilceler;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            preparedStatement.close();
            rs.close();
            con.close();
            dbConnection.baglantiSonlandir();
        }
        return null;
    }


}
