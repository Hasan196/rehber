/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorel.rehber.bo.boimpl;

import com.vektorel.rehber.vo.CinsiyetVO;
import com.vektorel.rehber.bo.CinsiyetBO;
import com.vektorel.rehber.db.DBConnection;
import com.vektorel.rehber.db.TException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hasan
 */
public class CinsiyetBOImpl implements CinsiyetBO {

    public Vector<CinsiyetVO> getAll() throws SQLException, TException {
        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM GNL_CINSIYET";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            rs = preparedStatement.executeQuery();
            Vector<CinsiyetVO> cinsiyetler = new Vector <CinsiyetVO>();

            while (rs.next()) {
                CinsiyetVO cinsiyet = new CinsiyetVO();
                cinsiyet.setKod(rs.getString("kod"));
                cinsiyetler.add(cinsiyet);
            }
            return cinsiyetler;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            preparedStatement.close();
            rs.close();
            con.close();
            dbConnection.baglantiSonlandir();
        }
        return null;
    }

    public CinsiyetVO getByKod(String kod)  throws SQLException, TException  {
        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM GNL_CINSIYET WHERE KOD=?";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, kod);
            rs = preparedStatement.executeQuery();
            CinsiyetVO cinsiyet = new CinsiyetVO();
            while (rs.next()) {              
                cinsiyet.setKod(rs.getString("kod"));
                cinsiyet.setId(Integer.valueOf(rs.getString("id")));
                cinsiyet.setTanim(rs.getString("tanim"));
            }
            return cinsiyet;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            preparedStatement.close();
            rs.close();
            con.close();
            dbConnection.baglantiSonlandir();
        }
        return null;
    }


}
