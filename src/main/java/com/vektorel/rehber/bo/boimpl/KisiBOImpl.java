package com.vektorel.rehber.bo.boimpl;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.util.List;

import com.vektorel.rehber.vo.KisiVO;
import com.vektorel.rehber.bo.KisiBO;
import com.vektorel.rehber.db.DBConnection;
import com.vektorel.rehber.db.TException;
import com.vektorel.rehber.vo.IlVO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KisiBOImpl implements KisiBO {

    @Override
    public void addKisi(String ad, String soyad, String il,
            String ilce, String cinsiyet, Date dogumTarihi, String telNo, String adres) throws SQLException, TException {
        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO GNL_KISI"
                + "( AD, SOYAD, IL_ID,ILCE_ID,CINSIYET_ID,DOGUM_TARIHI,tel_no,adres) VALUES"
                + "(?,?,?,?,?,?,?,?)";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();

            preparedStatement = con.prepareStatement(sql);

            preparedStatement.setString(1, ad);
            preparedStatement.setString(2, soyad);
            preparedStatement.setInt(3, Integer.valueOf(il));
            preparedStatement.setInt(4, Integer.valueOf(ilce));
            preparedStatement.setInt(5, Integer.valueOf(cinsiyet));
            preparedStatement.setDate(6, (java.sql.Date) dogumTarihi);
            preparedStatement.setString(7, telNo);
            preparedStatement.setString(8, adres);

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            preparedStatement.close();
            con.close();
            dbConnection.baglantiSonlandir();
        }
    }

    @Override
    public void updateKisi(KisiVO kisi) {

        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE GNL_KISI SET"
                + " AD=?, SOYAD=?, IL_ID=?,ILCE_ID=?,CINSIYET_ID=?,DOGUM_TARIHI=?,tel_no=?,adres=? where id=?";
        try {
                dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();

            preparedStatement = con.prepareStatement(sql);

            preparedStatement.setString(1, kisi.getAd());
            preparedStatement.setString(2, kisi.getSoyad());
            preparedStatement.setInt(3, Integer.valueOf(kisi.getIl()));
            preparedStatement.setInt(4, Integer.valueOf(kisi.getIlce()));
            preparedStatement.setInt(5, Integer.valueOf(kisi.getCinsiyet()));
            preparedStatement.setDate(6, (java.sql.Date) kisi.getDogumTarihi());
            preparedStatement.setString(7, kisi.getTelNo());
            preparedStatement.setString(8, kisi.getAdres());
            preparedStatement.setInt(9, kisi.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                preparedStatement.close();
                con.close();
                dbConnection.baglantiSonlandir();
            } catch (SQLException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public void deleteKisi(KisiVO kisi) {

        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM GNL_KISI WHERE id=?";
        try {
                dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();

            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, kisi.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                preparedStatement.close();
                con.close();
                dbConnection.baglantiSonlandir();
            } catch (SQLException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public Vector<KisiVO> getAll() throws SQLException, TException {
        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT kisi.id,kisi.ad,kisi.soyad,kisi.adres,kisi.dogum_tarihi,kisi.tel_no,il.kod as il,ilce.kod as ilce,cinsiyet.kod as cinsiyet "
                + "FROM GNL_KISI kisi,GNL_IL il , GNL_ILCE ilce,GNL_CINSIYET cinsiyet where "
                + "kisi.il_id = il.id and kisi.ilce_id = ilce.id and "
                + "kisi.cinsiyet_id = cinsiyet.id";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            rs = preparedStatement.executeQuery();
            Vector<KisiVO> kisiler = new Vector<KisiVO>();

            while (rs.next()) {
                KisiVO kisi = new KisiVO();
                kisi.setId(Integer.valueOf(rs.getString("id")));
                kisi.setAd(rs.getString("ad"));
                kisi.setSoyad(rs.getString("soyad"));
                kisi.setCinsiyet(rs.getString("cinsiyet"));
                kisi.setIl(rs.getString("il"));
                kisi.setIlce(rs.getString("ilce"));
                kisi.setTelNo(rs.getString("tel_no"));
                kisi.setAdres(rs.getString("adres"));
                kisi.setDogumTarihi(java.sql.Date.valueOf(rs.getString("dogum_tarihi")));
                kisiler.add(kisi);
            }
            return kisiler;

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            preparedStatement.close();
            rs.close();
            con.close();
            dbConnection.baglantiSonlandir();
        }
        return null;
    }

    @Override
    public ResultSet getAllResultSet() throws SQLException, TException {
        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM GNL_KISI";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            rs = preparedStatement.executeQuery();
            return rs;

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            preparedStatement.close();
            rs.close();
            con.close();
            dbConnection.baglantiSonlandir();
        }
        return null;
    }

    @Override
    public KisiVO getById(long id) {

        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String sql = "SELECT kisi.id,kisi.ad,kisi.soyad,kisi.adres,kisi.dogum_tarihi,kisi.tel_no,il.kod as il,ilce.kod as ilce,cinsiyet.kod as cinsiyet "
                + "FROM GNL_KISI kisi,GNL_IL il , GNL_ILCE ilce,GNL_CINSIYET cinsiyet where "
                + "kisi.il_id = il.id and kisi.ilce_id = ilce.id and "
                + "kisi.cinsiyet_id = cinsiyet.id and kisi.id=?";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, (int) id);
            rs = preparedStatement.executeQuery();
            KisiVO kisi = new KisiVO();
            while (rs.next()) {
                kisi.setId(Integer.valueOf(rs.getString("id")));
                kisi.setAd(rs.getString("ad"));
                kisi.setSoyad(rs.getString("soyad"));
                kisi.setCinsiyet(rs.getString("cinsiyet"));
                kisi.setIl(rs.getString("il"));
                kisi.setIlce(rs.getString("ilce"));
                kisi.setTelNo(rs.getString("tel_no"));
                kisi.setAdres(rs.getString("adres"));
                kisi.setDogumTarihi(java.sql.Date.valueOf(rs.getString("dogum_tarihi")));
            }
            return kisi;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                con.close();
                dbConnection.baglantiSonlandir();
            } catch (SQLException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public  Vector<KisiVO>  arama(String kelime) {

        Connection con = null;
        DBConnection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null; 	

        String sql = "SELECT kisi.id,kisi.ad,kisi.soyad,kisi.adres,kisi.dogum_tarihi,kisi.tel_no,il.kod as il,ilce.kod as ilce,cinsiyet.kod as cinsiyet "
                + "FROM GNL_KISI kisi,GNL_IL il , GNL_ILCE ilce,GNL_CINSIYET cinsiyet where "
                + "kisi.il_id = il.id and kisi.ilce_id = ilce.id and "
                + "kisi.cinsiyet_id = cinsiyet.id  and (kisi.ad=? or kisi.soyad=?)";
        try {
            dbConnection = new DBConnection();
            con = dbConnection.baglantiBaslat();
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, kelime);
            preparedStatement.setString(2, kelime);
            rs = preparedStatement.executeQuery();
              Vector<KisiVO> kisiler = new Vector<KisiVO>();

            while (rs.next()) {
                KisiVO kisi = new KisiVO();
                kisi.setId(Integer.valueOf(rs.getString("id")));
                kisi.setAd(rs.getString("ad"));
                kisi.setSoyad(rs.getString("soyad"));
                kisi.setCinsiyet(rs.getString("cinsiyet"));
                kisi.setIl(rs.getString("il"));
                kisi.setIlce(rs.getString("ilce"));
                kisi.setTelNo(rs.getString("tel_no"));
                kisi.setAdres(rs.getString("adres"));
                kisi.setDogumTarihi(java.sql.Date.valueOf(rs.getString("dogum_tarihi")));
                kisiler.add(kisi);
            }
            return kisiler;

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            try {
                throw new TException("Veritabanına Bağlanılamadı");
            } catch (TException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                con.close();
                dbConnection.baglantiSonlandir();
            } catch (SQLException ex) {
                Logger.getLogger(KisiBOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

}
