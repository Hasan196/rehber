/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorel.rehber.bo;

import com.vektorel.rehber.db.TException;
import com.vektorel.rehber.vo.IlceVO;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Hasan
 */
public interface IlceBO {
	public Vector<IlceVO> getAll() throws SQLException,TException;
	public IlceVO getByKod(String kod)  throws SQLException, TException ;    
}
